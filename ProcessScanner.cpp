//==================================================================================================================================
#include "Required.h"
//==================================================================================================================================
#include "ProcessScanner.h"
//==================================================================================================================================
CProcessScanner::CProcessScanner()
{
	m_ProcessID = new DWORD [ MAX_PROCESS_IDS ];

	SYSTEM_INFO	SystemInfo;

	GetNativeSystemInfo ( &SystemInfo );

	if ( SystemInfo.wProcessorArchitecture == PROCESSOR_ARCHITECTURE_AMD64
		|| SystemInfo.wProcessorArchitecture == PROCESSOR_ARCHITECTURE_IA64 )
	{
		m_Is64Bit = true;
	}


}
//==================================================================================================================================
CProcessScanner::~CProcessScanner()
{
	delete [] m_ProcessID;
}
//==================================================================================================================================
bool CProcessScanner::EnumerateModules ( HANDLE Process )
{
	wchar_t CurrentModulePath [ 1024 ];
	wchar_t CurrentModuleName [ MAX_PATH ];
	wchar_t LastModuleName [ MAX_PATH ];

	DWORD_PTR Address = 0;

	SYSTEM_INFO SysInfo;

	MEMORY_BASIC_INFORMATION MBI = {0};

	GetSystemInfo ( &SysInfo );

	memset ( CurrentModulePath, 0, sizeof ( CurrentModulePath ) );
	memset ( CurrentModuleName, 0, sizeof ( CurrentModuleName ) );
	memset ( LastModuleName, 0, sizeof ( LastModuleName ) );


	while ( Address < ( DWORD_PTR )SysInfo.lpMaximumApplicationAddress )
	{
		if ( VirtualQueryEx ( Process, ( LPCVOID )Address, &MBI, sizeof ( MEMORY_BASIC_INFORMATION ) ) )
		{
			if ( MBI.State & MEM_COMMIT && MBI.Type & MEM_IMAGE )
			{
				if ( GetMappedFileNameW ( Process, MBI.BaseAddress, CurrentModulePath, sizeof ( CurrentModulePath ) ) )
				{
					if ( _wcsicmp ( CurrentModuleName, LastModuleName ) ) // fresh module
					{
						// clean up the path and call CreateFile on it, then ReadFile


					}
				
					wcsncpy_s ( LastModuleName, CurrentModuleName, sizeof ( LastModuleName ) );
				}
			}

			Address += MBI.RegionSize;
		}
		else
		{
			Address += SysInfo.dwPageSize;
		}
	}

	return true;
}
//==================================================================================================================================
bool CProcessScanner::EnumerateThreads ( HANDLE Process )
{


	return true;
}
//==================================================================================================================================
bool CProcessScanner::Is64BitOperatingSystem()
{
	return m_Is64Bit;
}
//==================================================================================================================================
bool CProcessScanner::EnumerateAndScanProcesses()
{	
	DWORD Needed;

	HANDLE Process = INVALID_HANDLE_VALUE;

	if ( !EnumProcesses ( m_ProcessID, MAX_PROCESS_IDS, &Needed ) )
	{
		return false;
	}

	for ( size_t i = 0; i < ( Needed / sizeof ( DWORD ) ); i++ )
	{
		Process = OpenProcess ( PROCESS_VM_READ | PROCESS_VM_OPERATION | PROCESS_QUERY_INFORMATION, FALSE, m_ProcessID [ i ] );

		if ( Process != INVALID_HANDLE_VALUE )
		{
			EnumerateModules ( Process );

			CloseHandle ( Process );
		}
		else
		{
			// throw exception
		}

	}

	return true;
}
//==================================================================================================================================
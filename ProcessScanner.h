//==================================================================================================================================
#define MAX_PROCESS_IDS 4096
//==================================================================================================================================
typedef struct
{
	wchar_t m_BaseName [ MAX_PATH ];

	DWORD_PTR m_BaseAddress;
	DWORD m_Size;
	
	// sha checksum

}Module_t;
//==================================================================================================================================
typedef struct
{
	DWORD_PTR m_BaseAddress;

	DWORD m_ThreadID;
}Thread_t;
//==================================================================================================================================
typedef struct
{
	wchar_t m_BaseName [ MAX_PATH ];

	DWORD m_ProcessID;

	std::vector<Module_t> m_ModuleInformation;
	std::vector<Thread_t> m_ThreadInformation;
}Process_t;
//==================================================================================================================================
class CProcessScanner
{
public:

	CProcessScanner();
	~CProcessScanner();

	bool EnumerateAndScanProcesses();
	bool EnumerateModules ( HANDLE Process );
	bool EnumerateThreads ( HANDLE Process );
	
	bool Is64BitOperatingSystem();
	bool IsProcessWOW64 ( HANDLE Process );

private:
		
	bool m_Is64Bit;

	DWORD* m_ProcessID;

	std::vector<Process_t> m_ProcessInformation;
};
//==================================================================================================================================